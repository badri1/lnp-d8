(function ($) {

  Drupal.behaviors.highlight_js = {
    attach: function (context, settings) {
      //Process all code tags
      $('code').each(function (i, block) {
        if (block) {
          hljs.highlightBlock(block);
        }
      });
    }
  };

})(jQuery)

