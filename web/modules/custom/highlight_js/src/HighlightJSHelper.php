<?php

namespace Drupal\highlight_js;

use Drupal\Component\Utility\Unicode;

class HighlightJSHelper {
  /**
   * Get Configuration name.
   * @return string
   */
  public static function getConfigName() {
    return 'highlight_js.settings';
  }

  /**
   * Get module's Configuration object.
   */
  public static function getConfig() {
    $config = \Drupal::configFactory()
      ->getEditable(self::getConfigName());
    return $config;
  }


  /**
   * List the available themes.
   */
  public static function getListThemes() {

    $lib_dir = "libraries/highlightjs";
    $themes = array();

    $directory = $lib_dir . '/styles/';
    if (!empty($directory)) {
      $files = file_scan_directory($directory, '/.*\.css$/', array('key' => 'name'));
      foreach ($files as $key => $fileinfo) {
        $themes[Unicode::strtolower($key)] = Unicode::ucfirst($key);
      }
      natcasesort($themes);
    }

    return $themes;
  }
}