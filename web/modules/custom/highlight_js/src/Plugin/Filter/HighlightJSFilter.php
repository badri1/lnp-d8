<?php

namespace Drupal\highlight_js\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * A filter that adds a test attribute to any configured HTML tags.
 * Allows users to post code verbatim using &lt;code&gt; and &lt;?php ?&gt; tags.
 *
 * @Filter(
 *   id = "highlight_js",
 *   title = @Translation("Highlight JS Filter"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE,
 *   settings = {
 *     "tags" = {},
 *   },
 *   weight = -10
 * )
 */
class HighlightJSFilter extends FilterBase {

  /**
   * Prepare callback for Highlight JS filter.
   */
  public function prepare($text, $langcode) {
    $text = preg_replace_callback('@^<\?php(.+?)\?>@sm', '_highlight_js_filter_escape_code_tag_callback', $text);
    $text = preg_replace_callback('@^<code>(.+?)</code>@sm', '_highlight_js_filter_escape_code_tag_callback', $text);
    $text = preg_replace_callback('@^<code class="(.+?)">(.+?)</code>@sm', '_highlight_js_filter_escape_code_tag_lang_callback', $text);
    return $text;
  }


  /**
   * {@inheritdoc}
   */

  public function process($text, $langcode) {
    $text = preg_replace_callback('@\[highlightjs_code\](.+?)\[/highlightjs_code\]@s', '_highlight_js_filter_process_code_callback', $text);
    $text = preg_replace_callback('@\[highlightjs_code class="(.+?)"\](.+?)\[/highlightjs_code\]@s', '_highlight_js_filter_process_code_lang_callback', $text);
    //    $document = Html::load($text);
    //    foreach ($this->settings['tags'] as $tag) {
    //      $tag_elements = $document->getElementsByTagName($tag);
    //      foreach ($tag_elements as $tag_element) {
    //        $tag_element->setAttribute('test_attribute', 'test attribute value');
    //      }
    //    }
    //
    return new FilterProcessResult($text);


    //    $document = Html::load($text);
    //    foreach ($this->settings['tags'] as $tag) {
    //      $tag_elements = $document->getElementsByTagName($tag);
    //      foreach ($tag_elements as $tag_element) {
    //        $tag_element->setAttribute('test_attribute', 'test attribute value');
    //      }
    //    }
    //    return new FilterProcessResult(Html::serialize($document));
  }

  public function settingsForm(array $form, FormStateInterface $form_state) {
    //    $form['theme'] = array(
    //      '#type' => 'checkbox',
    //      '#title' => t('Theme'),
    //      '#default_value' => 0,
    //    );
    //
    //    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    if ($long) {
      return t('To post pieces of code, surround them with &lt;code&gt;...&lt;/code&gt; tags.');
    }
    else {
      return t('You may post code using &lt;code&gt;...&lt;/code&gt; (generic) tags.');
    }
  }


}
