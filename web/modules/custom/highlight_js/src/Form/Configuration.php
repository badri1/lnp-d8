<?php

namespace Drupal\highlight_js\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\highlight_js\HighlightJSHelper;

/**
 * Configuration form.
 */
class Configuration extends ConfigFormBase {

  /**
   * Get Form ID.
   */
  public function getFormId() {
    return 'highlight_js_settings_form';
  }

  /**
   * Build Form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);
    $themes = HighlightJSHelper::getListThemes();

    $config = HighlightJSHelper::getConfig();

    $form['theme'] = array(
      '#type' => 'select',
      '#title' => t('Theme'),
      '#options' => $themes,
      '#default_value' => $config->get('theme'),
    );

    $form['select_mode'] = array(
      '#type' => 'select',
      '#title' => t('Selection mode'),
      '#options' => [
        0 => t('Highlight JS Default @code', ['@code' => '<pre><code>']),
        'code' => t('Code @code', ['@code' => '<code>']),
      ],
      '#default_value' => $config->get('select_mode'),
    );


    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = HighlightJSHelper::getConfig();
    $config->set('theme', $form_state->getValue('theme'));
    $config->set('select_mode', $form_state->getValue('select_mode'));
    $config->save();

    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      HighlightJSHelper::getConfigName(),
    ];
  }

}
