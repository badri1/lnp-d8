<?php

namespace Drupal\lakshminp_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'SidebarSubscribeBlock' block.
 *
 * @Block(
 *  id = "sidebar_subscribe",
 *  admin_label = @Translation("Sidebar subscribe"),
 * )
 */
class SidebarSubscribeBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['sidebar_subscribe']['#markup'] = "<h2>Subscribe</h2><a class='use-ajax' data-dialog-options='{&quot;width&quot;:800}' data-dialog-type='modal' href='/subscribe' id='subscribe-button'>
  Download Code
</a>";

    return $build;
  }

}
