<?php

namespace Drupal\lakshminp_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'BottomSubscribeBlock' block.
 *
 * @Block(
 *  id = "bottom_subscribe",
 *  admin_label = @Translation("Bottom subscribe"),
 * )
 */
class BottomSubscribeBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['bottom_subscribe']['#markup'] = "
<div class='p-6 bg-yellow-lighter mt-6'>
<h2>Subscribe and download code</h2>
      <div class='py-6'>
    Want to download the code used in this blog post? Please <a class='use-ajax' data-dialog-options='{&quot;width&quot;:800}' data-dialog-type='modal' href='/subscribe'>subscribe</a> to my newsletter in which I send my latest posts and tutorials.
      </div>
      <a class='use-ajax' data-dialog-options='{&quot;width&quot;:800}' data-dialog-type='modal' href='/subscribe' id='subscribe-button'>
  Subscribe
</a>
</div>";

    return $build;
  }

}
