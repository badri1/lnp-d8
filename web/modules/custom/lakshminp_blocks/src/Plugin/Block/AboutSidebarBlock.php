<?php

namespace Drupal\lakshminp_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'AboutSidebarBlock' block.
 *
 * @Block(
 *  id = "about_sidebar",
 *  admin_label = @Translation("About sidebar block"),
 * )
 */
class AboutSidebarBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['about_sidebar']['#markup'] = '<div class="max-w-xs overflow-hidden md\:mt-24">
  <img src="https://pbs.twimg.com/profile_images/791145685722816512/q7P30Tyr_bigger.jpg" alt="Lakshmi Narasimhan">
  <div>
    <div class="font-bold text-xl mb-2">About me</div>
    <p class="text-grey-darker text-base">
      Hi there! I\'m Lakshmi Narasimhan, a full-stack developer with a devops flavour. I write lots of stuff here mainly about devops, some of which you might find useful.
      I\'m passonate about writing and using software to automate software, you know devops, continuous integration, TDD and the whole 9 yards. <a href="/about">more about me</a>
    </p>
  </div>
</div>';

    return $build;
  }

}
